package com.kyriba.osgispring.producer;

import com.kyriba.osgispring.consumer.Consumer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class Producer implements Runnable {

    private Consumer consumer;

    @Autowired
    public Producer(Consumer consumer) {
        this.consumer = consumer;
        new Thread(this).start();
    }

    public void run() {
        sleep();
        int messageNumber = 0;
        while (messageNumber <= 10) {
            consumer.consume("Hello from spring bundle : " + messageNumber++);
            sleep();
        }
    }

    void sleep() {
        try {
            Thread.sleep(100);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
