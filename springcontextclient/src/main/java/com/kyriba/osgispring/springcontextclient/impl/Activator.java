package com.kyriba.osgispring.springcontextclient.impl;

import org.apache.felix.dm.DependencyManager;
import org.apache.felix.dm.lambda.DependencyManagerActivator;
import org.osgi.framework.BundleContext;
import org.springframework.context.ApplicationContext;

import java.util.Arrays;

public class Activator extends DependencyManagerActivator {

    ApplicationContext springContext;

    @Override
    protected void init(BundleContext bundleContext, DependencyManager dependencyManager) throws Exception {
        component(comp -> comp.impl(this)
            .withSvc(ApplicationContext.class, true)
        );
    }

    private void start() {
        StringBuilder builder = new StringBuilder();
        builder.append("Spring beans: ")
                .append("\n");
        Arrays.stream(springContext.getBeanDefinitionNames())
                .forEach(b -> builder.append(b).append('\n'));
        System.out.println(builder.toString());
    }
}
