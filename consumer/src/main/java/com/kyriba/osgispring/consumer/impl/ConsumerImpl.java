package com.kyriba.osgispring.consumer.impl;

import com.kyriba.osgispring.consumer.Consumer;

public class ConsumerImpl implements Consumer {
    @Override
    public void consume(String message) {
        System.out.println("Message received: " + message);
    }
}