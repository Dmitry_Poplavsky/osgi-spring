package com.kyriba.osgispring.consumer;

public interface Consumer {
    void consume(String message);
}
