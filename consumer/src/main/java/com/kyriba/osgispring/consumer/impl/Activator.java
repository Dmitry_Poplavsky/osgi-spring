package com.kyriba.osgispring.consumer.impl;

import com.kyriba.osgispring.consumer.Consumer;
import org.apache.felix.dm.DependencyManager;
import org.apache.felix.dm.lambda.DependencyManagerActivator;
import org.osgi.framework.BundleContext;

public class Activator extends DependencyManagerActivator {
    @Override
    protected void init(BundleContext bundleContext, DependencyManager dependencyManager) {
        component(comp -> comp.impl(new ConsumerImpl()).provides(Consumer.class));
    }
}
