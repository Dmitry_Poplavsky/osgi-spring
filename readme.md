The goal is to deploy spring bundle context in osgi.

**consumer** - pure osgi bundle with Activator as entry point. Provides Consumer interface implementation.

**springproducer** - bundle with spring context that inject Consumer

**springcontextclient** - bundle that injects spring context from springproducer and prints all beans

META-INF.spring
 - osgi-context.xml - describes osgi services that bundle will inject/provide
 - spring-context.xml - describes spring context for spring bundle
 
gemini-blueprint-extender bundle is used to detect and configure Spring bundles (that includes META-INF.spring)

**Versions**

gradle 3.1 java 1.8


artifactoryUser, artifactoryPassword, artifactoryMaven (jforg artifactory) should be provided as gradle properties



**How to run:**

gradle clean build run - to start felix with project bundles
